
<!doctype html>
<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
   
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

  <br/>
  <div class="container">
    <div class="row">

      <h3>Record payment</h3>
      <br/>
        <table class="table table-responsive">
            <thead>
              <tr>
                <th scope="col">Customer Name</th>
                <th scope="col">Amount Received</th>
                <th scope="col">Bank Charges (if any)</th>
                <th scope="col">Payment Date</th>
                <th scope="col">Payment</th>
                <th scope="col">Payment Mode</th>
                <th scope="col">CODEPYMTYP</th>
                <th scope="col">CNTBTCH</th>
                <th scope="col">CNTITEM</th>
                <th scope="col">IDRMIT</th>
                <th scope="col">IDCUST</th>
                <th scope="col">DATERMIT</th>
                <th scope="col">TEXTRMIT</th>
                <th scope="col">TXTRMITREF</th>
                <th scope="col">AMTRMIT</th>
                <th scope="col">AMTRMITTC</th>
                <th scope="col">RATEEXCHTC</th>
                <th scope="col">SWRATETC</th>
                <th scope="col">CNTPAYMETR</th>
                <th scope="col">AMTPAYMTC</th>
                <th scope="col">AMTDISCTC</th>
                <th scope="col">CODEPAYM</th>
                <th scope="col">CODECURN</th>
                <th scope="col">RATETYPEHC</th>
                <th scope="col">RATEEXCHHC</th>
                <th scope="col">SWRATEHC</th>
                <th scope="col">RMITTYPE</th>
                <th scope="col">DOCTYPE</th>
                <th scope="col">IDINVCMTCH</th>
                <th scope="col">CNTLSTLINE</th>
                <th scope="col">FISCYR</th>
                <th scope="col">FISCPER</th>
                <th scope="col">TEXTPAYOR</th>
                <th scope="col">DATERATETC</th>
                <th scope="col">RATETYPETC</th>
                <th scope="col">AMTADJENT</th>
                <th scope="col">DATERATEHC</th>
                <th scope="col">PAYMTYPE</th>
                <th scope="col">REMUNAPLTC</th>
                <th scope="col">REMUNAPL</th>
                <th scope="col">AMTRMITHC</th>
                <th scope="col">DOCNBR</th>
                <th scope="col">AMTADJHC</th>
                <th scope="col">OPERBANK</th>
                <th scope="col">OPERCUST</th>
                <th scope="col">AMTDISCHC</th>
                <th scope="col">AMTDBADJHC</th>
                <th scope="col">AMTCRADJHC</th>
                <th scope="col">AMTDBADJTC</th>
                <th scope="col">AMTCRADJTC</th>
                <th scope="col">SWJOB</th>
                <th scope="col">APPLYMETH</th>
                
              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
            </tbody>
          </table>


    </div>
</div>


</body>
</html>
