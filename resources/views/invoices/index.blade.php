
<!doctype html>
<html>
<head>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
   
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

  <br/>
  <div class="container">
    <div class="row">

      <h3>Invoices</h3>
      <br/>
        <table class="table table-responsive">
            <thead>
              <tr>
                <th scope="col">Customer Name</th>
                <th scope="col">Invoice</th>
                <th scope="col">Order Number</th>
                <th scope="col">Invoice Date</th>
                <th scope="col">Subject</th>
                <th scope="col">Cntbtch</th>
                <th scope="col">Cntitem</th>
                <th scope="col">Idcust</th>
                <th scope="col">Idinvc</th>
                <th scope="col">Idshpt</th>
                <th scope="col">Texttrx</th>
                <th scope="col">Idtrx</th>
                <th scope="col">Ordrnbr</th>   
                <th scope="col">Custpo</th>
                <th scope="col">Invcdesc</th>
                <th scope="col">Idacctset</th>
                <th scope="col">Dateinvc</th>
                <th scope="col">Fiscyr</th>
                <th scope="col">Fiscper</th>
                <th scope="col">Codecurn</th>
                <th scope="col">Exchratehc</th>
                <th scope="col">Termcode</th>
                <th scope="col">Datedue</th>
                <th scope="col">Datedisc</th>
                <th scope="col">Pctdisc</th>
                <th scope="col">Amtdiscavl</th>
                <th scope="col">Codeslsp1</th>
                <th scope="col">Codeslsp2</th>
                <th scope="col">Pctsasplt1</th>
                <th scope="col">Pctsasplt2</th>
                <th scope="col">Email</th>
                <th scope="col">Ctacphone</th>
                <th scope="col">Ctacfax</th>
                <th scope="col">Ctacemail</th>
                <th scope="col">Amtdsbwtax</th>
                <th scope="col">Amtdsbntax</th>
                <th scope="col">Amtdscbase</th>
                <th scope="col">Invctype</th>
                <th scope="col">Idshipnbr</th>
                <th scope="col">Idshipnbr</th>
                <th scope="col">Enteredby</th>
                <th scope="col">Datebus </th>
                <th scope="col">Cntbtch</th>
                <th scope="col">Cntitem</th>
                <th scope="col">Cntline</th>
                <th scope="col">Iditem</th>
                <th scope="col">Iddist</th>
                <th scope="col">Textdesc</th>
                <th scope="col">Unitmeas</th>
                <th scope="col">Qtyinvc</th>
                <th scope="col">Amtcost</th>
                <th scope="col">Amtpric</th>
                <th scope="col">Amtextn</th>
                <th scope="col">Amtcogs</th>
                <th scope="col">Tottax</th>
                <th scope="col">Ratetax1</th>
                <th scope="col">Ratetax2</th>
                <th scope="col">Cntbtch</th>
                <th scope="col">Cntitem</th>
                <th scope="col">Cntpaym</th>
                <th scope="col">Datedue</th>
                <th scope="col">Amtdue</th>
                <th scope="col">Datedisc</th>
                <th scope="col">Amtdisc</th>
                <th scope="col">Amtduehc</th>
                <th scope="col">Amtdischc</th>
                <th scope="col">Cntbtch</th>
                <th scope="col">Ctitem</th>
                <th scope="col">Cntline</th>
                <th scope="col">Optfield</th>
                <th scope="col">Value</th>

              </tr>
            </thead>
            <tbody>
              <tr>
                <th scope="row">1</th>
                <td>Mark</td>
                <td>Otto</td>
                <td>@mdo</td>
              </tr>
            </tbody>
          </table>


    </div>
</div>


</body>
</html>
