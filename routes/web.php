<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PaymentsController; 
use App\Http\Controllers\InvoicesController; 
use App\Http\Controllers\OrganizationController; 

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/payments', [PaymentsController::class, 'index']);
Route::get('/invoices', [InvoicesController::class, 'index']);
Route::post('/save_customer', [OrganizationController::class, 'save']);
