<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Response;
use GuzzleHttp\RequestOptions;
use App\Models\Organization;


class OrganizationController extends Controller
{
    public function save( Request $request){
        
        $data = $request->data;
        $request_decode = base64_decode($data,true);
        $data_decode = json_decode($request_decode);

        $organization = Organization::create([
            'name' => $data_decode->data->Company_Name,
            'customer_id' => $data_decode->data->Customer_Id,
            'token' => "token",
            'status' =>  "0",
            'response' => base64_decode($data,true),
        ]);

        return response()->json([
            "message" => "Se han guardado los datos del cliente en la base de datos"
        ], 200);

    }
}
